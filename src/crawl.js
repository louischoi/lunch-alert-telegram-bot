const config = require('config');
const _ = require('lodash');
const request = require('request-promise');
const cheerio = require('cheerio');

module.exports = () => {
  const promises = _.map(config, ({ URL = '' }, STORE_NAME) => {
    const option = {
      uri: URL,
      transform: body => cheerio.load(body),
    };

    return request(option)
      .then($ => ({
        store: STORE_NAME,
        html: $.text(),
      }))
      .catch(() => ({
        store: STORE_NAME,
        html: '',
      }));
  });

  let matches = [];
  return Promise.all(promises).then((stores) => {
    _.forEach(stores, ({ store, html }) => {
      const wanted = config[store].ALERT_ON || [];
      // TODO use full sentence on menu
      const founds = wanted.filter(food => html.includes(food)).map(food => ({ store, food }));
      matches = matches.concat(founds);
    });
    return matches;
  })
    .then(() => matches);
};
