const crawl = require('./crawl');
const render = require('./render');
const send = require('./send');

Promise.resolve().then(async () => {
  const matches = await crawl();
  const rendered = render(matches);
  await send(process.argv[2], process.argv[3], rendered);
});
