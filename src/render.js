const fs = require('fs');
const Mustache = require('mustache');

module.exports = (substitutions) => {
  let template = '';
  try {
    template = fs.readFileSync(`${__dirname}/template.hbs`).toString();
  } catch (e) {
    throw new Error('template not found');
  }

  return Mustache.render(template, substitutions);
};
