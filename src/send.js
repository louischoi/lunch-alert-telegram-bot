const request = require('request-promise');

module.exports = async (botToken, chatId, text) => {
  if (!botToken || !chatId) {
    throw new Error('invalid channel credentials');
  }

  const option = {
    method: 'POST',
    uri: `https://api.telegram.org/bot${botToken}/sendMessage`,
    json: true,
    body: {
      chat_id: chatId,
      text,
      parse_mode: 'Markdown',
    },
  };

  return request(option);
};
